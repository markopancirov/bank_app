How to run:

1) Install Grails from http://grails.org/index.html  
2) Clone the repository  
3) Create a new MySQL database  
4) Edit bank_app/grails-app/conf/application.yml file and put your database credentials under environments -> development -> dataSource  
 - username, password and also database name in the connection url string  
5) Change directory in the command line to bank_app root project folder  
6) Run the application with the Grails command: run-app  
7) Go to http://localhost:8080/ in your browser  
8) There are 3 pre-defined users:  
 - username: Marko, password: 12345, role: CLIENT  
 - username: Pero, password: lozinka, role: CLIENT  
 - username: banka, password: banka, role: EMPLOYEE  
you can also register new CLIENT users  
9) On bootstrap when these users are created or when you register a new client user,
you will see a message in the command line that will tell you the account number of the
created user so you can easily know to which account numbers you can transfers funds to