<html>
<head>
    <meta name="layout" content="main"/>
    <title>My Orders</title>
</head>
<body>

<div id="content" role="main">

    <div class="text-center padding-around">
        <g:form name="statusForm" controller="client">
            <g:checkBox name="setChecked" value="${setChecked}" />SET
            <g:checkBox name="completeChecked" value="${completeChecked}" />COMPLETE
            <g:checkBox name="declinedChecked" value="${declinedChecked}" />DECLINED
            <g:actionSubmit value="FILTER" action="filter" class="btn btn-primary"/>
        </g:form>
    </div>

    <div class="container" style="margin-bottom: 25px;">

        <g:if test="${orders.size() > 0}">
            <div class="table-responsive ">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Account Number</th>
                        <th>Receiver Name</th>
                        <th>Date & Time</th>
                        <th>Amount</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>

                    <g:each var="order" in="${orders}">
                            <td>${order.receiverAccount.number}</td>
                            <td>${order.receiverAccount.user.username}</td>
                            <td><g:formatDate date="${order.dateCreated}" type="datetime" style="MEDIUM"/></td>
                            <td>${order.amount}</td>
                            <td>${order.status}</td>
                        </tr>
                    </g:each>

                    </tbody>
                </table>
            </div>

        </g:if>

        <g:if test="${orders.size() == 0}">
            <div class="alert alert-info" style="margin: 0px; padding: 0px; padding-left: 5px;">
                <strong>No orders to show</strong>
            </div>
        </g:if>

    </div>

</div>
</body>
</html>