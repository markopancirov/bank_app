package bank_app


class BootStrap {

    ManagerUsersService managerUsersService
    ConstantsService constantsService

    def init = { servletContext ->

        def user1 = new User('Marko', '12345')
        managerUsersService.registerUser(user1, constantsService.ROLE_CLIENT)

        def user2 = new User('Pero', 'lozinka')
        managerUsersService.registerUser(user2, constantsService.ROLE_CLIENT)

        def user3 = new User('banka', 'banka')
        managerUsersService.registerUser(user3, constantsService.ROLE_EMPLOYEE)
    }
    def destroy = {
    }
}
