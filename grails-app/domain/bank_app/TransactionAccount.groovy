package bank_app

class TransactionAccount {

    static hasMany = [bankOrders: BankOrder]
    String number
    double balance
    User user

    TransactionAccount(number, balance, User user) {
        this.number = number
        this.balance = balance
        this.user = user
    }

    static constraints = {
        number(nullable: false, blank: false, num: true, unique: true, validator:{num ->

            if(num ==~ /^\d{10}$/) {
                return true
            }
            else {
                return "invalidFormat"
            }
        })
    }


}
