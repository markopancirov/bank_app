package bank_app

import grails.gorm.transactions.Transactional

/**
 * Constants Service
 */
@Transactional
class ConstantsService {

    static final USER_EXISTS = -2
    static final USER_REGISTER_SUCCESS = 1
    static final USER_REGISTER_FAIL = -1
    static final USER_VALIDATE_FAIL = 0
    static final ROLE_CLIENT = "ROLE_CLIENT"
    static final ROLE_EMPLOYEE = "ROLE_EMPLOYEE"
}
