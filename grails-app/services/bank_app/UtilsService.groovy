package bank_app

import grails.gorm.transactions.Transactional

/**
 * Utility service
 */
@Transactional
class UtilsService {

    /**
     * Generate 'unique & random' 10 digit account number
     * @return generated account number
     */
    def generateUniqueAccNumber() {
        Random random = new Random();
        long num = (long) Math.floor(random.nextDouble() * 9000000000L) + 1000000000;
        while (TransactionAccount.findByNumber(num.toString())) {
            num =  (long) Math.floor(random.nextDouble() * 9000000000L) + 1000000000;
        }

        return num.toString();
    }
}
