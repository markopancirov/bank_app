package bank_app

import grails.plugin.springsecurity.annotation.Secured

/**
 * A controller for ROLE_EMPLOYEE user's view and model
 */
class EmployeeController {

    ManageAccountsService manageAccountsService

    def setChecked = true
    def completeChecked = true
    def declinedChecked = true

    /**
     * Filter system's Bank Orders based on order status
     */
    @Secured("hasRole('ROLE_EMPLOYEE')")
    def filter() {
        setChecked = params.setChecked
        completeChecked = params.completeChecked
        declinedChecked = params.declinedChecked
        redirect(action: "index")
    }

    /**
     * Generate sorted system orders table based on order status filters
     */
    @Secured("hasRole('ROLE_EMPLOYEE')")
    def index() {
        def orders = BankOrder.listOrderByDateCreated(order: "asc").findAll {
            (setChecked? it.status == Status.SET : false) ||
                    (completeChecked? it.status == Status.COMPLETE : false) ||
                    (declinedChecked? it.status == Status.DECLINED : false)
        }

        render(view: "index", model: [orders: orders, setChecked: setChecked, completeChecked: completeChecked, declinedChecked: declinedChecked])
    }

    /**
     * Generate order detail view
     */
    @Secured("hasRole('ROLE_EMPLOYEE')")
    def order() {
        def order = BankOrder.get(params.id)
        render(view: "order", model: [order: order])
    }

    /**
     * Approve order
     */
    @Secured("hasRole('ROLE_EMPLOYEE')")
    def approveOrder() {
        flash.message = manageAccountsService.approveOrder(BankOrder.get(params.id))
        redirect(action: "order", id: params.id)
    }

    /**
     * Decline order
     */
    @Secured("hasRole('ROLE_EMPLOYEE')")
    def declineOrder () {
        flash.message = manageAccountsService.declineOrder(BankOrder.get(params.id))
        redirect(action: "order", id: params.id)
    }
}
