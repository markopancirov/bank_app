package bank_app

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured

/**
 * A controller for ROLE_CLIENT user's view and model
 */
class ClientController {

    SpringSecurityService springSecurityService
    ManageAccountsService manageAccountsService

    def setChecked = true
    def completeChecked = true
    def declinedChecked = true

    /**
     * Filter client user's Bank Orders based on order status
     */
    @Secured("hasRole('ROLE_CLIENT')")
    def filter() {
        setChecked = params.setChecked
        completeChecked = params.completeChecked
        declinedChecked = params.declinedChecked
        redirect(action: "index")
    }

    /**
     * Generate sorted client user orders table based on order status filters
     */
    @Secured("hasRole('ROLE_CLIENT')")
    def index() {

        User currentUser = springSecurityService.currentUser
        def orders = currentUser.transactionAccount.bankOrders.sort{it.dateCreated}.findAll {
            (setChecked? it.status == Status.SET : false) ||
            (completeChecked? it.status == Status.COMPLETE : false) ||
            (declinedChecked? it.status == Status.DECLINED : false)
        }

        render(view: "index", model: [orders: orders, setChecked: setChecked, completeChecked: completeChecked, declinedChecked: declinedChecked])
    }

    /**
     * Render make order view
     */
    @Secured("hasRole('ROLE_CLIENT')")
    def makeOder() {
        render(view: "makeOrder")
    }

    /**
     * Create order
     */
    @Secured("hasRole('ROLE_CLIENT')")
    def createOrder() {

        if(params.amount.empty || params.receiverAccNumber.empty) {
            flash.message = "Receiver account number and amount can't be empty"
            render(view: "makeOrder")
            return
        }

        User currentUser = springSecurityService.currentUser
        double amount = Double.parseDouble(params.amount)
        TransactionAccount senderAcc = currentUser.transactionAccount
        TransactionAccount receiverAcc = TransactionAccount.findByNumber(params.receiverAccNumber.toString())

        def responseMsg = manageAccountsService.createOrder(senderAcc, receiverAcc, amount);
        flash.message = responseMsg
        render(view: "makeOrder")
    }
}
