<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="Grails"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />

    <asset:stylesheet src="application.css"/>

    <g:layoutHead/>
</head>
<body>

    <div class="navbar navbar-default navbar-static-top text-center navig">

        <div id="accountOptions">
            <sec:ifNotLoggedIn>
                <g:link controller='login' action='auth' class="hover">Login</g:link>
                <g:link controller='register' action='index' class="hover">Register</g:link>
            </sec:ifNotLoggedIn>

            <sec:ifLoggedIn>
                <h2 class="text-primary"><sec:loggedInUserInfo field='username'/></h2>
                <g:link controller='logout' class="hover">Logout</g:link>
            </sec:ifLoggedIn>
        </div>

        <a href="${createLink(controller: 'home')}">
            <asset:image src="logoBank.png" alt="Logo" width="180"/>
        </a>

        <div class="text-center tbuttons">

            <a class="btn btn-primary" role="button" href="${createLink(controller: 'home', action: "index")}">
                Home
            </a>

            <sec:ifAllGranted roles='ROLE_CLIENT'>

                <a class="btn btn-primary" role="button" href="${createLink(controller: 'client', action: "makeOder")}">
                    Make an order
                </a>

                <a class="btn btn-primary" role="button" href="${createLink(controller: 'client', action: "index")}">
                    My Orders
                </a>

            </sec:ifAllGranted>

            <sec:ifAllGranted roles='ROLE_EMPLOYEE'>

                <a class="btn btn-primary" role="button" href="${createLink(controller: 'employee', action: "index")}">
                    Orders
                </a>

            </sec:ifAllGranted>
        </div>

    </div>


    <g:layoutBody/>

    <div class="footer navbar-fixed-bottom text-center" role="contentinfo">
        Developed by Marko Pancirov
    </div>

    <asset:javascript src="application.js"/>

</body>
</html>
