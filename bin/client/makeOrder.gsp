<html>
<head>
    <meta name="layout" content="main"/>
    <title>Create Order</title>
</head>
<body>

<div id="content" role="main" class="text-center padding">

    <h2 class="tip">Transfer Funds</h2>

    <g:form >
        <label class="margin-top">Receiver Account number</label><br>
        <input type="text" name='receiverAccNumber' maxlength="10" />
        <br>
        <label class="margin-top">Amount to Transfer</label><br>
        <input type="number" name='amount' class="currency" min="0.01" step="0.01"  />
        <g:actionSubmit value="CREATE ORDER" action="createOrder" class="btn btn-primary margin-top"/>
    </g:form>

    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>

</div>
</body>
</html>