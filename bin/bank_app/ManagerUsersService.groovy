package bank_app

import grails.gorm.transactions.Transactional

/**
 * Manage Useres Service
 */
@Transactional
class ManagerUsersService {

    ConstantsService constantsService
    UtilsService utilsService

    /**
     * Register new user with a given role
     * @param user user
     * @param role user role
     * @return constant
     */
    def registerUser(User user, String role) {

        if(User.findByUsername(user.username)) {
            return constantsService.USER_EXISTS
        }

        if(role == constantsService.ROLE_CLIENT) {
            TransactionAccount newUserAcc = new TransactionAccount(utilsService.generateUniqueAccNumber(), 1000, user)
            user.transactionAccount = newUserAcc
        }

        if(user.validate()) {
            if (user.save(flush: true)) {

                def r = Role.findOrSaveByAuthority(role)
                RoleGroup rg
                if(role == constantsService.ROLE_CLIENT) {
                    rg = RoleGroup.findOrSaveByName("GROUP_USERS")
                } else {
                    rg = RoleGroup.findOrSaveByName("GROUP_EMPLOYEES")
                }

                RoleGroupRole.create(rg, r, true)
                UserRoleGroup.create(user, rg, true)

                UserRole.withSession {
                    it.flush()
                    it.clear()
                }
                return constantsService.USER_REGISTER_SUCCESS
            } else {
                return constantsService.USER_REGISTER_FAIL
            }
        } else {
            user.errors.allErrors.reverse().each {
                println it
            }
            return constantsService.USER_VALIDATE_FAIL
        }
    }
}
