package bank_app


class BankOrder {

    Date dateCreated
    TransactionAccount senderAccount
    TransactionAccount receiverAccount
    Status status
    double amount

    def isReadyToComplete () {
        def count = bank_app.BankOrder.count()
        if ((count % 10 == 0 && count > 0) || amount >= 100) {
            return false
        } else {
            return true
        }
    }

    BankOrder(TransactionAccount sender, TransactionAccount receiver, Status status, double amount){
        this.status = status
        this.senderAccount = sender
        this.receiverAccount = receiver
        this.amount = amount
    }

    static constraints = {
        status enumType: 'ordinal'
    }
}
