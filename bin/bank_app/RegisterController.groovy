package bank_app

import grails.plugin.springsecurity.annotation.Secured

/**
 * Controller for registering new client users
 */
class RegisterController {

    ManagerUsersService managerUsersService
    ConstantsService constantsService

    /**
     * Display register form
     */
    @Secured(value=["hasRole('ROLE_ANONYMOUS')"])
    def index() { }

    /**
     * Register new user
     */

    @Secured(value=["hasRole('ROLE_ANONYMOUS')"])
    def register() {
        def user = new User(params)

        def result = managerUsersService.registerUser(user, constantsService.ROLE_CLIENT)

        switch (result) {
            case constantsService.USER_REGISTER_SUCCESS:
                redirect controller: "client"
                break;
            case constantsService.USER_REGISTER_FAIL:
                flash.message = 'Something went wrong'
                redirect(action: "index")
                break;
            case constantsService.USER_VALIDATE_FAIL:
                flash.message = 'Username and password must not be empty'
                redirect(action: "index")
                break;
            case constantsService.USER_EXISTS:
                flash.message = 'User with that username already exists'
                redirect(action: "index")
                break;
            default:
                flash.message = 'Something went wrong'
                redirect(action: "index")
        }
    }
}
