package bank_app

import groovy.transform.CompileStatic

/**
 * Enum representing 3 states in which a bank order can be: COMPLETE - Izvršen, SET -Zadan, DECLINED - Odbijen
 */
@CompileStatic
enum Status {
    DECLINED(-1),
    SET(0),
    COMPLETE(1),

    final int id
    private Status(int id) { this.id = id }
}
