package bank_app

import grails.gorm.transactions.Transactional

/**
 * Manage Accounts Service
 */
@Transactional
class ManageAccountsService {

    /**
     * Transfer funds for the given bank order - if transfer is successful order status is
     * set to COMPLETE, if transfer fails order status is set to DECLINED
     * @param bankOrder bank order
     * @return true if transfer was successful, false otherwise
     */
    def transferFunds(BankOrder bankOrder) {

        TransactionAccount sender = bankOrder.senderAccount
        TransactionAccount receiver = bankOrder.receiverAccount
        double amount = bankOrder.amount

        TransactionAccount.withTransaction { status ->

            if (sender.balance >= amount && bankOrder.status == Status.SET) {

                sender.balance -= amount
                receiver.balance += amount

                bankOrder.status = Status.COMPLETE
                bankOrder.save(flush: true)
                return true


            } else {
                bankOrder.status = Status.DECLINED
                bankOrder.save(flush: true)
                return false
            }
        }
    }

    /**
     * Create new bank order with sender transaction account, receiver transaction account and amount
     * @param senderAcc sender transaction account
     * @param receiverAcc receiver transaction account
     * @param amount order amount
     * @return response message - I know this is not good practice, I didn't have time to create all those constants and then to process them in the controller...
     */
    def createOrder(TransactionAccount senderAcc, TransactionAccount receiverAcc, double amount) {
        if (senderAcc.balance >= amount) {

            if(receiverAcc) {

                def bankOrder = new BankOrder(senderAcc, receiverAcc, Status.SET, amount)

                if(bankOrder.validate()) {

                    if (bankOrder.save(flush: true)) {

                        if(bankOrder.isReadyToComplete()) {

                            def result = transferFunds(bankOrder)
                            if(result) {
                                return "Funds transferred successfully"
                            } else {
                                return "Transfer failed"
                            }

                        } else {
                            return "Order has been set"
                        }

                    } else {
                        bankOrder.errors.allErrors.reverse().each {
                            println it
                        }
                        return "Something went wrong"
                    }

                } else {
                    bankOrder.errors.allErrors.reverse().each {
                        println it
                    }
                    return "Order not right"
                }

            }
            else {
                return "Receiver account number doesn't exist"
            }

        } else {
            return "Transfer amount larger than balance"
        }
    }

    /**
     * Declined bank order
     * @param bankOrder bank order
     * @return response message
     */
    def declineOrder(BankOrder bankOrder) {
        bankOrder.status = Status.DECLINED
        if(bankOrder.save(flush: true)) {
            return "Order successfully declined"
        } else {
            bankOrder.errors.allErrors.reverse().each {
                println it
            }
            return "Order couldn't be declined"
        }
    }

    /**
     * Approve bank order
     * @param bankOrder bank order
     * @return response message
     */
    def approveOrder(BankOrder bankOrder) {
        def result = transferFunds(bankOrder)
        if(result) {
            return "Order successfully approved - funds transferred"
        } else {
            return "Order couldn't be approved - Order declined!"
        }
    }
}
