package bank_app

import grails.plugin.springsecurity.annotation.Secured

/**
 * Controller used to display home page
 */
class HomeController {

    @Secured("permitAll")
    def index() {
    }
}
