<html>
<head>
    <meta name="layout" content="main"/>
    <title>Register</title>
</head>
<body>

<div id="content" role="main" class="text-center padding-around">

    <h2 class="tip">Register</h2>

    <g:form action="register" >
        <div class="margin-top"><label>Username: </label><g:textField name="username" /></div>
        <div class="margin-top"><label>Password: </label><g:textField name="password" /></div>
        <g:actionSubmit value="Register" class="btn btn-primary big-margin"/>
    </g:form>

    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>

</div>
</body>
</html>