<%@ page import="bank_app.Status" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Order Detail</title>

</head>
<body>

<div id="content" role="main">

    <div class="container" style="margin-bottom: 25px;">

        <ul class="list-group text-center section">
            <li class="list-group-item row detailRow"><div class="col-md-6 bg-primary field">Sender Username</div><div class="col-md-6 field">${order.senderAccount.user.username}</div></li>
            <li class="list-group-item row detailRow"><div class="col-md-6 bg-primary field">Sender Acc Number</div><div class="col-md-6 field">${order.senderAccount.number}</div></li>
            <li class="list-group-item row detailRow"><div class="col-md-6 bg-primary field">Receiver Username</div><div class="col-md-6 field">${order.receiverAccount.user.username}</div></li>
            <li class="list-group-item row detailRow"><div class="col-md-6 bg-primary field">Receiver Acc Number</div><div class="col-md-6 field">${order.receiverAccount.number}</div></li>
            <li class="list-group-item row detailRow"><div class="col-md-6 bg-primary field">Order Amount</div><div class="col-md-6 field">${order.amount}</div></li>
            <li class="list-group-item row detailRow"><div class="col-md-6 bg-primary field">Order Status</div><div class="col-md-6 field">${order.status}</div></li>
            <li class="list-group-item row detailRow"><div class="col-md-6 bg-primary field">Order Created</div><div class="col-md-6 field"><g:formatDate date="${order.dateCreated}" type="datetime" style="MEDIUM"/></div></li>
        </ul>

        <g:if test="${order.status == bank_app.Status.SET}">
            <div class="text-center tbuttons">
                <a class="btn btn-danger" role="button" href="${createLink(action: "declineOrder", id: order.id)}">
                    Decline
                </a>

                <a class="btn btn-primary" role="button" href="${createLink(action: 'approveOrder', id: order.id)}">
                    Approve
                </a>
            </div>
        </g:if>

        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>

    </div>

</div>
</body>
</html>